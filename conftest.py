import pytest
import os

from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions

host = os.environ.get('MAIN_HOST')
port = os.environ.get('MAIN_PORT')

@pytest.fixture()
def driver():
    options = ChromeOptions()
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    options.add_argument("--disable-blink-features=AutomationControlled")
    options.add_argument("--no-sandbox")
    options.add_argument("--headless")
    server = f'http://{host}:{port}/wd/hub'
    driver = webdriver.Remote(command_executor=server, options=options)
    yield driver
    driver.quit()
