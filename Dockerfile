FROM python:3.12-slim-bullseye

WORKDIR /app

COPY . .

RUN pip3 install -r requirements.txt

CMD pytest -s -v -m smoke --alluredir=/app/allure-results tests/
